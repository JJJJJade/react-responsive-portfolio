import './Testimonial.scss'
import {ScreenHeading} from '../../utilities/ScreenHeading/ScreenHeading'
import ScrollService from '../../utilities/ScrollService'
import Animations from '../../utilities/Animations'
import OwlCarousel from 'react-owl-carousel'
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import shape from "../../../src/assets/Projects/shape-bg.png";

export const Testimonial = (props) => {

    let fadeInScreenHandler = (screen) => {
        if (screen.fadeInScreen !== props.id) return;
        Animations.animations.fadeInScreen(props.id);
    };
    const fadeInSubscription =
        ScrollService.currentScreenFadeIn.subscribe(fadeInScreenHandler);

    const options = {
        loop: true,
        margin: 0,
        nav: true,
        animateIn: "bounceInRight",
        animateOut: "bounceOutRight",
        dots: true,
        autoplay: true,
        smartSpeed: 1000,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 1,
            },
            1000: {
                items: 3,
            },
        },
    };

    return <div>
        <ScreenHeading title={"Projects"} subHeading={"My work in web applications"}/>
        <section className='projects-section fade-in' id={props.id || ''}>
            <div className='container'>
                <div className='row'>
                    <OwlCarousel className='owl-carousel' id='projects-carousel' {...options}>
                        <div className='col-lg-12'>
                            <div className='testi-item'>
                                <div className='testi-comment'>
                                    <p>
                                        <i className='fa fa-quote-left'></i>
                                        An e-commerce furniture platform SPA by using React, React-Redux, Redux-Form, with
                                        (Express + NodeJS) as REST API backend to talk to database (MySQL)
                                        <i className='fa fa-quote-right'></i>
                                    </p>
                                    <ul>
                                        <li>
                                            Back-end: TypeORM, MySQL
                                        </li>
                                        <li>
                                            Front-end: React, Redux, MUI
                                        </li>

                                    </ul>
                                </div>
                                <div className='project-info'>
                                    <img src="project1.png" alt="No Internet"/>
                                    <h5>Herman Miller Shopping Mall</h5>
                                </div>
                            </div>
                        </div>
                    </OwlCarousel>
                </div>
            </div>
        </section>
        <div className="footer-image">
            <img src={shape} alt="Photo not responding" />
        </div>
    </div>

}