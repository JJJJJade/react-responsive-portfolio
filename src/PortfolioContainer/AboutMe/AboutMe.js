import './AboutMe.scss'
import {ScreenHeading} from '../../utilities/ScreenHeading/ScreenHeading'
import ScrollService from '../../utilities/ScrollService'
import Animations from '../../utilities/Animations'
import {aboutMeDescription} from "../../helper";


export const AboutMe = (props) => {

    let fadeInScreenHandler = (screen) => {
        if (screen.fadeInScreen !== props.id) {
            return
        }
        Animations.animations.fadeInScreen(props.id)
    }

    const fadeInSubscription = ScrollService.currentScreenFadeIn.subscribe(fadeInScreenHandler)

    const SCREEN_CONSTANTS = {
        description: aboutMeDescription,
        highlights: {
            bullets: [
                "Full Stack Web and Mobile Development",
                "React, Redux, NextJS programming",
                "Building REST APis",
                "Managing Databases",
                "Interactive and Responsive Front End design"
            ],
            heading:"Here are a Few Highlights"
        },
    }

    const renderHighlight = () => {
        return SCREEN_CONSTANTS.highlights.bullets.map((value,i) => {
            return <div className='highlight' key={i}>
                <div className='highlight-blob'></div>
                <span>{value}</span>
            </div>
        })
    }

    return <div className='about-me-container screen-container' id={props.id || ""}>
        <div className='about-me-parent'>
            <ScreenHeading title={'About Me'} subHeading={'Why Choose Me?'} />
            <div className='about-me-card'>
                <div className='about-me-profile'>

                </div>
                <div className='about-me-details'>
                    <span className='about-me-description'>{SCREEN_CONSTANTS.description}</span>
                    <div className='about-me-highlights'>
                        <div className='highlight-heading'>
                            <span>{SCREEN_CONSTANTS.highlights.heading}</span>
                        </div>
                        {renderHighlight()}
                    </div>
                    <div className='about-me-options'>
                        <button className='btn primary-btn' onClick={() => ScrollService.scrollHandler.scrollToHireMe()}>
                            {''}
                            Hire Me {' '}
                        </button>
                        <a href='SiqiYan-resume.pdf' download='Siqi Yan.pdf'>
                            <button className='btn highlighted-btn'>Get Resume</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>

}


