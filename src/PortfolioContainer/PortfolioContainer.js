import {TOTAL_SCREENS} from "../utilities/commonUtils";


export const PortfolioContainer = () => {
    const mapAllScreens = () => {
        return (
            TOTAL_SCREENS.map((s)=>{
                return (s.component) ?
                    <s.component screenName={s.screen_name} key = {s.screen_name} id = {s.screen_name}/> :
                    <div key={s.screen_name}></div>
        }))
    }

    return <div className='portfolio-container'>
        {mapAllScreens()}

    </div>
}