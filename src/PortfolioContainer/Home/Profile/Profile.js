import {githubURL, gitlabURL, googleURL, linkedinURL} from "../../../helper";
import Typical from 'react-typical'
import './Profile.scss'
import ScrollService from '../../../utilities/ScrollService'

export const Profile = () => {
    return <div className='profile-container'>
        <div className='profile-parent'>
            <div className='profile-details'>
                <div className='colz'>
                    <div className='colz-icon'>
                        <a href={linkedinURL}>
                            <i className='fa fa-linkedin-square'></i>
                        </a>
                        <a href={googleURL}>
                            <i className='fa fa-google-plus-square'></i>
                        </a>
                        <a href={gitlabURL}>
                            <i className='fa fa-gitlab'></i>
                        </a>
                        <a href={githubURL}>
                            <i className='fa fa-github-square'></i>
                        </a>
                    </div>
                </div>
                <div className='profile-details-name'>
                    <span className='primary-text'>
                        {" "}
                        Hello, I'M <span className='highlighted-text'>Jade</span>
                    </span>
                </div>
                <div className='profile-details-role'>
                    <span className='primary-text'>
                        {" "}
                        <h1>
                            {" "}
                            <Typical
                            loop={Infinity}
                            steps = {[
                                "Enthusiastic Dev 💪",
                                2000,
                                "Full Stack Developer 👩‍💻",
                                2000,
                                "Cross Platform 📱💻",
                                2000,
                                "React/Redux/NextJS Dev ",
                                2000,
                            ]}
                            />
                        </h1>
                        <span className='profile-role-tagline'>
                            A focused and motivated full-stack web developer
                        </span>
                    </span>
                </div>
                <div className='profile-options'>
                    <button onClick={() => ScrollService.scrollHandler.scrollToHireMe()} className='btn primary-btn'>
                        {''}
                        Hire Me {' '}
                    </button>
                    <a href='SiqiYan-resume.pdf' download='Siqi Yan.pdf'>
                        <button className='btn highlighted-btn'>Get Resume</button>
                    </a>
                </div>
            </div>
            <div className='profile-picture'>
                <div className='profile-picture-background'>

                </div>
            </div>
        </div>
    </div>
}