import './Home.scss'
import {Profile} from "./Profile/Profile";
import {Footer} from "./Footer/Footer";
import {Header} from "./Header/Header";

export const Home = () => {
    return  <div className='home-container'>
        <Header/>
        <Profile/>
        <Footer/>
    </div>
}