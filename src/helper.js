
export const linkedinURL = 'https://www.linkedin.com/in/siqi-yan-197618149/'
export const gitlabURL = 'https://gitlab.com/JJJJJade'
export const githubURL = 'https://github.com/JJJJJJade'
export const googleURL = 'siqiyan97@gmail.com'

export const aboutMeDescription = "A self motivated full stack developer with\n" +
    "hands-on project experiences, designing and\n" +
    "building products from scratch. My goal is to\n" +
    "build web applications with both visually appealing\n" +
    "and user experience-oriented functionality."